<?php
header("Access-Control-Allow-Origin: *"); 
header("Access-Control-Allow-Methods: *");  
header("Access-Control-Allow-Headers: Content-Type");


$servername = "localhost";
$username = "root";
$password = "94654";
$DBname = "sys";

$conn = new mysqli($servername, $username, $password, $DBname);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 





    $requestData = $_POST;
   //$requestData = json_decode(file_get_contents('php://input'), true);
    
    $errors = array();
   
        ValidateGroup($requestData['group']);
        ValidateName($requestData['name']);
        ValidateSurname($requestData['surname']);
        ValidateGender($requestData['gender']);
        ValidateBirth($requestData['birthday']);

        if($errors) {
            $response['errors'] = $errors;
            echo json_encode(['success' => false, 'errors' => $response['errors']]);
        }
     else {
        // Виклик функції для вставки даних в базу даних
        $Student_id = $requestData['studentID'];
         $group = $requestData['group'];
         $name = $requestData['name'];
         $surname = $requestData['surname'];
         $gender = $requestData['gender'];
         $birthday = $requestData['birthday'];

         if (EditStudent($Student_id, $group, $name, $surname, $gender, $birthday, $conn)) {
            echo json_encode(['success' => true]);
        } else {
            echo json_encode(['success' => false, 'errors' => ['Помилка при редагуванні даних до бази даних']]);
        }
        //echo json_encode(['success' => true]);
        }

function AddToDB($group_, $name_, $surname_, $gender_, $birthday_, $connect): bool {
      $sql = "INSERT INTO `user`(`group`, `name`, `surname`, `gender`, `birthday`) VALUES('$group_', '$name_', '$surname_', '$gender_', '$birthday_')";
     if($connect->query($sql) === TRUE) {
         return true;
     }
     else {
         return false;
     }
 }


function EditStudent($Student_id, $group_, $name_, $surname_, $gender_, $birthday_, $connect): bool {
    $sql = " UPDATE `user` SET `group`='$group_',`name`='$name_',`surname`='$surname_',`gender`='$gender_',`birthday`='$birthday_' WHERE `id` = '$Student_id'";
    if($connect->query($sql) === TRUE) {
        return true;
    }
    else {
        return false;
    }
 }


$conn->close();







function ValidateGroup($group) {
    global $errors;
    if($group == "") {
        $errors[] = 'Group is required';
        return false;
    }
    return true;
}

function ValidateName($name) {
    global $errors;
    if($name == "") {
        $errors[] = 'Name is required';
        return false;
    }
    else if(strlen($name) < 2) {
        $errors[] = 'Name must have more then 2 symbols';
        return false;
    }
    else if (!preg_match('/^[A-Z][a-z]+$/', $name)) {
        $errors[] = 'Name must start with a capital letter and contain only letters';
        return false;
    }
    return true;
}

function ValidateSurname($surname) {
    global $errors;
    if($surname == "") {
        $errors[] = 'Surname is required';
        return false;
    }
    else if(strlen($surname) < 2) {
        $errors[] = 'Surname must have more then 2 symbols';
        return false;
    }
    else if (!preg_match('/^[A-Z][a-z]+$/', $surname)) {
        $errors[] = 'Surname must start with a capital letter and contain only letters';
        return false;
    }
    return true;
}

function ValidateGender($gender) {
    global $errors;
    if($gender == "") {
        $errors[] = 'Gender is required';
        return false;
    }
    return true;
}

function ValidateBirth($birthday) {
    global $errors;
    if($birthday == "") {
        $errors[] = 'Birthday is required';
        return false;
    }
    else if(strtotime($birthday) > time()) {
        $errors[] = 'Birthdate cannot be in the future';
        return false;
    } 
    else if(strtotime($birthday) > strtotime('-16 years')) {
        $errors[] = 'You must be at least 16 years old';
        return false;
    } 
    else if(strtotime($birthday) < strtotime('-70 years')) {
        $errors[] = 'You must be no more than 70 years old';
        return false;
    }
    return true;
}
?>