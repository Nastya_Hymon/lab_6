const express = require('express')
const mongoose = require('mongoose');
const Room = require('./models/room')
const RoomMessage = require('./models/roomMessage')
const Task = require('./models/task')

const app = express();
app.set('view engine', 'ejs')
app.use(express.urlencoded({extended: true}))
app.use(express.static('public'))
const PORT = 3003
const HOST = 'localhost';


const http = require('http');
const socketIO = require('socket.io');


const server = http.createServer(app);
const io = socketIO(server);

// Підключення до бд
const url = 'mongodb+srv://anastasiiahymonpz2022:FRBRV7pfXDusULyj@mycluster.jnmm6h1.mongodb.net/?retryWrites=true&w=majority&appName=MyCluster';

mongoose
    .connect(url, { useNewUrlParser: true, useUnifiedTopology: true })
    .then((res)=> console.log("Connected to DB"))
    .catch((error)=> console.log(error));


let uname;
let all_users = [];




app.get('/', (req, res) => {
    res.render('registration')
})

app.get('/page', (req, res) => {
    res.render('page', {name: uname})
})

let users = {} 
app.post('/Registration', (req, res)=>{
    console.log(req.body)
    uname = req.body.uname
   
    all_users.push(req.body.uname)
    console.log(all_users)
    res.redirect('/page')
})


app.get('/chats', (req, res)=>{
  
    res.render('chat', {name: uname})

})

app.get('/tasks', (req, res)=>{
    res.render('tasks', {name:uname})
})

app.get('/student', (req, res)=>{
    res.render('page', {name: uname})
})


app.get('/Logout', (req, res)=>{
    res.render('registration')
})

app.get('/allRooms', (req, res) => {
    RoomMessage.find({}).then(function(allRooms) {
        res.json(allRooms)
        allRooms.forEach((room) => {
            rooms[room.roomName] = {users:{}};
            console.log(room.roomName);
        });
    }).catch((error)=> console.log(error))
   // res.json(data); // Відправка даних у форматі JSON
});  

app.get('/allTasks', (req, res) => {
    Task.find({}).then(function(allTasks) {
        res.json(allTasks)
        allTasks.forEach((task) => {
            console.log(task.Name);
        });
    }).catch((error)=> console.log(error))
   // res.json(data); // Відправка даних у форматі JSON
});  

let rooms = {};
let i = 0;
io.on('connection', socket => {
   // users[socket.id] = all_users[i];
   users[socket.id] = uname;
    ++i;
    let name = users[socket.id]
    //console.log(users)
   
    console.log('Клієнт підключився успішно');
    socket.on('send-chat-message', message =>{
        RoomMessage.findOneAndUpdate(
            { roomName: message.roomName },
            { $addToSet: { messages: { senderName: message.name, text:message.mess } } },
            { new: true, upsert: true }
        ).then(updatedRoom => {
            console.log(updatedRoom)
        }).catch(err => {
            console.error(err);
        });
        socket.broadcast.emit('add-notification', {message: message.mess, name: message.name, roomName: message.roomName});
       socket.to(message.roomName).emit('chat-message', {message: message.mess, name: message.name, roomName: message.roomName});
    })
    
    // socket.on('disconnect', () =>{
    //     socket.broadcast.emit('user-disconnect', users[socket.id]);
    //     console.log(`ds  ${users[socket.id]}`)
    //     delete rooms[]users[socket.id];
    //  })
     socket.on('disconnect', () => {
        getUserRooms(socket).forEach(room => {
          socket.to(room).emit('user-disconnect', rooms[room].users[socket.id])
          delete rooms[room].users[socket.id]
        })
      })


     socket.on('new-room', room =>{
        const roomToDB = new RoomMessage({roomName: room.roomName});
        roomToDB.save();
       

        rooms[room.roomName] = {users:{}};
        console.log(rooms)
        socket.broadcast.emit('add-new-room',room);
     } )

     
     socket.on('join-to-room', data =>{
        RoomMessage.findOneAndUpdate(
            { roomName: data.roomName, 'participants.userName': { $ne: data.userName } },
            { $addToSet: { participants: { userName: data.userName } } },
            { new: true, upsert: true }
        ).then(updatedRoom => {
            console.log(updatedRoom)
        }).catch(err => {
            console.error(err);
        });

      socket.join(data.roomName);
      rooms[data.roomName].users[socket.id] = data.userName
        console.log(data);
        console.log(rooms);
        socket.broadcast.emit('add-icon',data);
     } )



     //////////////////////////////////////////////////////////
     // таски
    socket.on('add-task', task =>{
        socket.broadcast.emit('add-new-task', {board: task.board, name: task.name, date: task.date, text: task.text});
        const newTask = new Task({
            Board: task.board,
            Name: task.name,
            Date: task.date,
            Description: task.text
        });
        newTask.save();
    })

    socket.on('edit-task', editedTask => {
        Task.findOneAndUpdate({Name: editedTask.name},
            {
                $set: {
                    Board: editedTask.board,
                    Date: editedTask.date,
                    Description: editedTask.text,
                  // Додайте тут інші поля, які потрібно оновити
                }
              },
              { new: true } // Повернути оновлений документ
        ).then(updatedTask => {
            if (!updatedTask) {
              console.log('Task not found');
            } else {
              console.log('Updated Task:', updatedTask);
            }
          })
          .catch(error => {
            console.error('Error updating task:', error);
          });
        
        console.log(editedTask)
        socket.broadcast.emit('update-task', editedTask);
    });
});

server.listen(PORT, HOST, () => {
    console.log(`Сервер запущений на: http://${HOST}:${PORT}`);
});

function getUserRooms(socket) {
    return Object.entries(rooms).reduce((names, [name, room]) => {
      if (room.users[socket.id] != null) names.push(name)
      return names
    }, [])
  }