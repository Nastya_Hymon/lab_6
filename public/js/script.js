$(document).ready(function() {
   
    var name = document.getElementById('nameUp');
name.textContent = sessionStorage.getItem('username');
});
// видалення рядка з таблиці

function DeleteElem() {
var deleteButtons = document.querySelectorAll('.btn-delete');

// Пройтися по кожній кнопці і додати обробник подій для кліків
deleteButtons.forEach(function(button) {
    //button.onclick = function() {
    button.addEventListener('click', function() {
    var row = button.parentNode.parentNode;

    var studentName = row.cells[3].textContent;

    // Вставити дані в текст модального вікна
    var modalBody = document.getElementById('confirm-text');
    modalBody.innerHTML = `<p>Are you sure you want to delete user <b>${studentName}</b>?</p>`;

    var confirmDeletion = document.getElementById("warning-modal");
      if(confirmDeletion.style.display !== "block") {
            confirmDeletion.style.display = "block";
          }
    });
       // }
});

var closeWindowDeletion =  document.querySelectorAll('.close')
closeWindowDeletion.forEach(function(button) {
    button.addEventListener('click', function() {
        document.getElementById("warning-modal").style.display = "none";
    });
});

var deleteButtons = document.querySelectorAll('.btn-delete');
deleteButtons.forEach(function(button) {
    button.onclick = function() {
   // button.addEventListener('click', function() {
        var studentID = this.parentNode.parentNode.querySelector('input[name="student-id"]').value;
        console.log(studentID);
        var rowToDelete = this.closest('tr');

        var confirmButton = document.getElementById('ok-button');
        //confirmButton.addEventListener('click', function() {
            confirmButton.onclick = function() {
            if (rowToDelete) {
                rowToDelete.remove();

                $.ajax({
                    url: 'http://localhost:3000/DeleteStudent.php',
                    type: 'POST',
                    //contentType: 'application/json', 
                    data: {studentID : studentID},
                   // data: JSON.stringify(data), // Перетворення об'єкта у JSON строку
                    success: function(response) {
                        try {
                            var parsedResponse = JSON.parse(response); // Розпарсити JSON відповідь
                            if (parsedResponse.success) {
                                console.log('Success:', parsedResponse);
                                document.getElementById("warning-modal").style.display = "none"
                            } 
                          
                     }
                    catch(error) {
                        console.error('Error parsing JSON:', error);
                        console.log('Response:', response);
                    }
                    },
                    error: function(xhr, status, error) {
                        console.error('Error sending request:', error);
                    }
                });
            }
        }
          
        //});
    //});
    }
});

// видалення    
}

///////////////////////////////////////////////////////////////////////////////////////////
//додавання
function CloseWindowAdd() {
    var close = document.getElementById("add-modal");
    if(close.style.display !== "none") {
        close.style.display = "none"
    }
}

function AddStudent() {
var showModalWindow = document.getElementById("add-student-btn");
var formTitle = document.getElementById("form-title");
showModalWindow.addEventListener('click',function(){
    formTitle.innerHTML = "Add student";
    btn_save = document.getElementById("ok-button-add");
    btn_save.innerHTML = "Ok";
    if(btn_save.textContent === "Ok"){
        document.getElementById("ok-button-edit").style.display = "none";
   }
   document.getElementById("ok-button-add").style.display = "inline";
   
    document.getElementById("group").value = "";
    document.getElementById("name").value = "";
    document.getElementById("surname").value = "";
    document.getElementById("gender").value = "M";
    document.getElementById("birthday").value = "";

    var Show = document.getElementById("add-modal");
    if(Show.style.display !== "block") {
        Show.style.display = "block";
    }
});



const studentsTable = document.getElementById("students-table");
const confirmAdding = document.getElementById("ok-button-add");

confirmAdding.addEventListener('click', function() {
    const id = document.getElementById("student-id").value;
    const group =  document.getElementById("group").value;
    const name = document.getElementById("name").value;
    const surname = document.getElementById("surname").value;
    const gender = document.getElementById("gender").value;
    const birthday = document.getElementById("birthday").value;

    const data = {
        id: id,
        group: group,
        name: name,
        surname: surname,
        gender: gender,
        birthday: birthday
    };
    
    handleFormSubmission(data); // Виклик функції для обробки ajax запиту та форми
});
}


function handleFormSubmission(data) {
    $.ajax({
    url: 'http://localhost:3000/server.php',
    type: 'POST',
    //contentType: 'application/json', 
    data: data,
   // data: JSON.stringify(data), // Перетворення об'єкта у JSON строку
    success: function(response) {
        try {
            var parsedResponse = JSON.parse(response); // Розпарсити JSON відповідь
            if (parsedResponse.success) {
                console.log('Success:', parsedResponse);
                if ($("#ok-button-add").is(":visible")) {
                //addStudentToTable(data);
                loadStudentsData();
                document.getElementById("add-modal").style.display = "none";
                }
            } 
            else {
                console.log("проблема при валідації");
                console.error('Failure:', parsedResponse.errors);
                for(var i = 0; i < parsedResponse.errors.length; i++) {
                    alert(parsedResponse.errors[i]);
                }
            }
     }
    catch(error) {
        console.error('Error parsing JSON:', error);
        console.log('Response:', response);
    }
    },
    error: function(xhr, status, error) {
        console.error('Error sending request:', error);
    }
});
}


function addStudentToTable(data) {
    const studentsTable = document.getElementById("students-table");
    
    const newRow = document.createElement('tr');
    newRow.innerHTML = ` <td>  
        <form>
            <label for="checkbox2" aria-label="Checkbox Label"></label>
            <input type="checkbox" id="checkbox2">
        </form>
    </td>
    <td>${data.group}</td>   
    <td>${data.name} ${data.surname}</td>
    <td>${data.gender}</td>
    <td>${data.birthday}</td>
    <td><div class="status-circle status-color"></div></td>
    <td>
        <button class="btn-edit"> <img class="img-pencil" src="/Images/Pencil.png" alt="pencil"></button>
        <button class="btn-delete"> <img class="img-delete" src="/Images/delete.png" alt="delete"> </button>
    </td>`;
    if(btn_save.textContent === "Ok"){
    studentsTable.appendChild(newRow);
    }

    var close = document.getElementById("add-modal");
    close.style.display = "none";


    // Додати обробники подій для нових кнопок видалення та редагування
    EditStudent();
    DeleteElem();
}


document.addEventListener("DOMContentLoaded", function() {
    loadStudentsData();
    AddStudent();
    DeleteElem();
    EditStudent();
});


// var username = localStorage.getItem('username'); // Отримати збережений логін користувача
// document.getElementById('nameUp').textContent = username;



 function EditStudent() {
    var editButtons = document.querySelectorAll(".btn-edit");
    // Додаємо обробник подій до кожної кнопки "Edit"
    editButtons.forEach(function(button) {
        button.addEventListener('click', function() {
           
            var formTitle = document.getElementById("form-title");
            formTitle.innerHTML = "Edit student";

            var btn_save = document.getElementById("ok-button-edit");
            btn_save.innerHTML = "Save";

            document.getElementById("ok-button-add").style.display = "none";
            document.getElementById("ok-button-edit").style.display = "inline";

            // Знаходимо ближній рядок до кнопки "Edit"
            var currentRow = this.closest('tr');
            var cells = currentRow.cells;
            // Отримуємо дані студента з поточного рядка
            var _group = cells[2].textContent;
            var _name = cells[3].textContent.split(' ')[0];
            var _surname = cells[3].textContent.split(' ')[1];
            var _gender = cells[4].textContent;
            var _birthday = cells[5].textContent;

            // Заповнюємо поля форми даними студента з поточного рядка
            document.getElementById("group").value = _group;
            document.getElementById("name").value = _name;
            document.getElementById("surname").value = _surname;
            document.getElementById("gender").value = _gender;
            document.getElementById("birthday").value = _birthday;

            var Show = document.getElementById("add-modal");
            Show.style.display = "block";
            var studentID = this.parentNode.parentNode.querySelector('input[name="student-id"]').value;
      
        btn_save.onclick = function() {
            //const studentID = document.getElementById("student-id").value;
            const group =  document.getElementById("group").value;
            const name = document.getElementById("name").value;
            const surname = document.getElementById("surname").value;
            const gender = document.getElementById("gender").value;
            const birthday = document.getElementById("birthday").value;
            //console.log(studentID);
            const data = {
                studentID: studentID,
                group: group,
                name: name,
                surname: surname,
                gender: gender,
                birthday: birthday
            };
           // console.log(data);
            $.ajax({
                url: 'http://localhost:3000/EditStudent.php',
                type: 'POST',
                //contentType: 'application/json', 
                data: data,
               // data: JSON.stringify(data), // Перетворення об'єкта у JSON строку
                success: function(response) {
                    var parsedResponse = JSON.parse(response); // Розпарсити JSON відповідь 
                    if (parsedResponse.success) {
                        console.log('Success:', parsedResponse);

                            loadStudentsData();
            
                             document.getElementById("add-modal").style.display = "none";
                            
                    } else {
                        console.log("проблема при валідації");
                        console.error('Failure:', parsedResponse.errors);
                        for(var i = 0; i < parsedResponse.errors.length; i++) {
                            alert(parsedResponse.errors[i]);
                        }
                    }
                },
                error: function(xhr, status, error) {
                    console.error('Error sending request:', error);
                }
            });
        };
    });   
  });
 }
 


 function loadStudentsData() {
    $.ajax({
        //TODO: add correct url
        url: 'http://localhost:3000/DB_functions.php',
        type: 'POST',
        //contentType: 'application/json', 
       
       // data: JSON.stringify(data), // Перетворення об'єкта у JSON строку
        success: function(response) {
            try {
            var parsedResponse = JSON.parse(response); // Розпарсити JSON відповідь
            if (parsedResponse.success) {
                var students = parsedResponse.students;
                var tableBody = $("#students-table-body");
                tableBody.empty(); // Очищаємо вміст тіла таблиці перед додаванням нових даних
                students.forEach(function(student) {
                    var row = "<tr>" +
                    "<td>" +
                    "<form>" +
                    "<label for='checkbox1' aria-label='Checkbox Label'></label>" +
                    "<input type='checkbox' id='checkbox1'>" +
                    "</form>" +
                    "</td>" +
                    "<td style='display: none;'>" + "<input type='hidden'id='student-id' name='student-id' value='" + student.id + "'>" + "</td>" +
                    "<td>" + student.group + "</td>" +
                    "<td>" + student.name + " " + student.surname + "</td>" +
                    "<td>" + student.gender + "</td>" +
                    "<td>" + student.birthday + "</td>" +
                    "<td><div class='status-circle'></div></td>" +
                    "<td>" +
                    "<button class='btn-edit'> <img class='img-pencil' src='/Images/Pencil.png' alt='pencil'></button>" +
                    "<button class='btn-delete'> <img class='img-delete' src='/Images/delete.png' alt='delete'> </button>" +
                    "</td>" +
                    "</tr>";
                    tableBody.append(row);
                    EditStudent();
                    DeleteElem();
                 
                });
            } else {
                console.error("Error loading students data");
            }
        } catch(error) {
            console.error('Error parsing JSON:', error);
            console.log('Response:', response);
        }
        },
        error: function(xhr, status, error) {
            console.error('Error sending request:', error);
        }
    });
}



