
 const Socket = io(); // Підключення до сокету на клієнтській стороні
var username = sessionStorage.getItem('username');
let AllRooms;
fetch('/allRooms')
    .then(response => response.json())
    .then(data => {
        AllRooms = data
        console.log('Data received from server:', data);
       
        // Тут ви можете виконати будь-які дії з отриманими даними
    })
    .catch(error => console.error('Error fetching data:', error));



function AddNotification(name, message) {
    const notPanel = document.getElementById('notificationPanel');
        const newNotification = `<li>
        <div class="user-info">
        <img src="/Images/massage-user-icon.png" alt="user">
        <span class="user-name">${name}</span>
        </div>
        <span class="message">${message}</span>
    </li>`;
    notPanel.innerHTML += newNotification;
}

Socket.on('add-notification', data =>{
   AllRooms.forEach(room => {
    if(room.roomName === data.roomName) {
        room.participants.forEach(participant => {
            console.log(participant.userName)
            console.log(data.name)
            if(participant.userName===data.name && data.name !=username) {
                AddNotification(data.name, data.message)
                }
        })
    }
   });
  
})