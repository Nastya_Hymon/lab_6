
const socket = io(); // Підключення до сокету на клієнтській стороні

// Tasks
$(document).ready(function() {
   
    var name = document.getElementById('nameUp');
name.textContent = sessionStorage.getItem('username');
});


let 
    tasksContainer = $("#tasksContainer");
    tasksForm = $("#tasksForm");

let tasksShortBlock = $(".task-block");

let controls = {
    add: $(".add-task-controls"),
    edit: $(".edit-task-controls")
}

let buttons = {
    addNew: $(".new-task-button"),
    confirmAdding: $("#addTaskButton"),
    save: $("#saveTaskButton"),
    cancel: $("#cancelTaskButton")
};

let fields = {
    board: $("#taskBoardField"),
    name: $("#taskNameField"),
    date: $("#taskDateField"),
    text: $("#taskTextField")
};

let boards = {
    toDo: $("#ToDoTaskboard"),
    inProcess: $("#InProcessTaskboard"),
    done: $("#DoneTaskboard")
};


// Додати нову задачу
buttons.addNew.off('click').on('click', function() {

   

    tasksContainer.addClass("unvisible").removeClass("visible");
    tasksForm.addClass("visible").removeClass("unvisible");
});

buttons.confirmAdding.on('click', (event) => {
    event.preventDefault();

    socket.emit('add-task', {board: fields.board.val(), name: fields.name.val(), date: fields.date.val(), text: fields.text.val()})

    let boardsName = "#" + fields.board.val();
    let tasksListBlock = $(boardsName).find('.tasks-list-block');

    tasksListBlock.append(`
        <div class="task-block d-flex justify-content-between m-2 p-2 border border-3 rounded">
            <span class="task-name">` + fields.name.val() + `</span>
            <span class="task-date">` + fields.date.val() + `</span>
            <span class="task-text">` + fields.text.val() + `</span>
        </div>
    `);



    tasksContainer.addClass("visible").removeClass("unvisible");
    tasksForm.addClass("unvisible").removeClass("visible");

    
    buttons.addNew.off('click').on('click', function() {
        tasksContainer.addClass("unvisible").removeClass("visible");
        tasksForm.addClass("visible").removeClass("unvisible");
    });
    
    // Очищення попередніх обробників кліку та встановлення нових
    ReloadEvents();
    ClearFields();
});

socket.on('add-new-task', data =>{
    let boardsName = "#" + data.board; 
    let tasksListBlock = $(boardsName).find('.tasks-list-block');

    tasksListBlock.append(`
    <div class="task-block d-flex justify-content-between m-2 p-2 border border-3 rounded">
        <span class="task-name">` + data.name + `</span>
        <span class="task-date">` + data.date + `</span>
        <span class="task-text">` + data.text + `</span>
    </div>
`);

ReloadEvents();
ClearFields();
})



// Редагувати задачу
function EditTasks(block) {

    currentTaskBlock = block;
    console.log(currentTaskBlock)

    tasksContainer.addClass("unvisible").removeClass("visible");
    tasksForm.addClass("visible").removeClass("unvisible");

    controls.add.addClass("unvisible").removeClass("visible");
    controls.edit.addClass("visible").removeClass("unvisible");

    // Визначення поточної дошки
    let board = currentTaskBlock.parent().parent();

    let old_Board = fields.board.val();

    let taskName = currentTaskBlock.find('.task-name').text();
    let taskDate = currentTaskBlock.find('.task-date').text();
    let taskText = currentTaskBlock.find('.task-text').text();

    fields.name.val(taskName);
    fields.date.val(taskDate);
    fields.text.val(taskText);

    buttons.save.off('click').on('click', (event) => {
        event.preventDefault();

       
        let editedTask = {

            board: fields.board.val(),
            oldBoard: old_Board,

            name: fields.name.val(),
            date: fields.date.val(),
            text: fields.text.val()
        };
    console.log(editedTask.block)
        // Відправка даних про редагування через сокет
        socket.emit('edit-task', editedTask);
       
        let boardsName = "#" + fields.board.val();


        // console.log($(boardsName)[0]);
        // console.log(board.get(0));

        if ($(boardsName)[0] === board.get(0)) {
            currentTaskBlock.find('.task-name').text(fields.name.val());
            currentTaskBlock.find('.task-date').text(fields.date.val());
            currentTaskBlock.find('.task-text').text(fields.text.val());
        }
        else {

            let boardsName = "#" + fields.board.val();
            let tasksListBlock = $(boardsName).find('.tasks-list-block');

            tasksListBlock.append(`
                <div class="task-block d-flex justify-content-between m-2 p-2 border border-3 rounded">
                    <span class="task-name">` + fields.name.val() + `</span>
                    <span class="task-date">` + fields.date.val() + `</span>
                    <span class="task-text">` + fields.text.val() + `</span>
                </div>
            `);

            currentTaskBlock.remove();

            // Очищення попередніх обробників кліку та встановлення нових
            ReloadEvents();
        }

        tasksContainer.addClass("visible").removeClass("unvisible");
        tasksForm.addClass("unvisible").removeClass("visible");

        buttons.addNew.off('click').on('click', function() {
            tasksContainer.addClass("unvisible").removeClass("visible");
            tasksForm.addClass("visible").removeClass("unvisible");
        });

        controls.add.addClass("visible").removeClass("unvisible");
        controls.edit.addClass("unvisible").removeClass("visible");

        ClearFields();
    });
};




buttons.cancel.on('click', function(event) {
    event.preventDefault();
    tasksContainer.addClass("visible").removeClass("unvisible");
    tasksForm.addClass("unvisible").removeClass("visible");
});

// Редагувати задачу
tasksShortBlock.off('click').on('click', function() {
    let block = $(this);
    EditTasks(block);
});

function ClearFields() {
    fields.name.val('');
    fields.date.val('');
    fields.text.val('');
}

function ReloadEvents() {
    tasksShortBlock = $(".task-block");
    tasksShortBlock.off('click').on('click', function() {

        
        
        let block = $(this);
        EditTasks(block);
    });
}



socket.on('update-task', updatedTask => {
   
   

    
    let taskToRemove =  $("#" + updatedTask.oldBoard).find('.tasks-list-block').find(`.task-name:contains("${updatedTask.name}")`).closest('.task-block');
    taskToRemove.remove(); // Видаліть об'єкт


    
    let tasksListBlock = $("#" + updatedTask.board).find('.tasks-list-block')
    let i = 0
    tasksListBlock.append(`
    <div id='task-id' class="task-block d-flex justify-content-between m-2 p-2 border border-3 rounded">
        <span class="task-name">` + updatedTask.name + `</span>
        <span class="task-date">` + updatedTask.date + `</span>
        <span class="task-text">` + updatedTask.text + `</span>
    </div>
    
`);
ReloadEvents();
ClearFields();
});



fetch('/allTasks')
    .then(response => response.json())
    .then(data => {
        AllTasks = data;
        console.log('Data received from server:', data);
        data.forEach(task => {
            let boardsName = "#" + task.Board; 
            let tasksListBlock = $(boardsName).find('.tasks-list-block');

            tasksListBlock.append(`
            <div class="task-block d-flex justify-content-between m-2 p-2 border border-3 rounded">
                <span class="task-name">` + task.Name + `</span>
                <span class="task-date">` + task.Date + `</span>
                <span class="task-text">` + task.Description + `</span>
            </div>
        `);

        })
        ReloadEvents();
        ClearFields();
        // Тут ви можете виконати будь-які дії з отриманими даними
    })
    .catch(error => console.error('Error fetching data:', error));