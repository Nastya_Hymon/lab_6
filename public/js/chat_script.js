const socket = io(); // Підключення до сокету на клієнтській стороні

const send_btn = document.getElementById('send-message-btn');
const message = document.getElementById('messageInput');

var username = sessionStorage.getItem('username'); // Отримати збережений логін користувача
//document.getElementById('nameUp').textContent = username;


// send_btn.onclick = function() {
// let mess = message.value;
// socket.emit('send-chat-message', {mess: mess, name:username})
// message.value = '';
// addMessageToChatMy(mess); 
// }

$(document).ready(function() {
    $('.left-text').each(function() {
        $(this).css('height', 'auto'); // Встановлюємо висоту на авто перед обчисленням
        var height = $(this).height();
        $(this).css('height', height + 'px'); // Встановлюємо обчислену висоту

    });
    var name = document.getElementById('nameUp');
    name.textContent = sessionStorage.getItem('username');
});


socket.on('connect', () => {
    console.log('Підключено до сервера');
});

socket.on('user-disconnect', name => {
    console.log(`Відключено до сервера ${name}`);
});


socket.on('chat-message', data =>{
   
    addMessageToChatSomeone(data)
})

function addMessageToChatSomeone(data) {
    const messageField = document.getElementById('message-field');

    // HTML-код для додавання
    const newMessage = `
        <div class="d-flex align-items-left">
            <div class="user-chat-image d-flex flex-column align-items-center">
                <img class="u-image-con" src="/Images/user-icon.png" alt="user">
                <p id="chat-user" class="ms-1">${data.name}</p>
            </div>
            <span class="ms-2 p-1 left-text">${data.message}</span>
        </div>
    `;

    // Додавання нового повідомлення в кінець блока
    messageField.innerHTML += newMessage;
}

function addMessageToChatSomeoneElse(name, message) {
    const messageField = document.getElementById('message-field');

    // HTML-код для додавання
    const newMessage = `
        <div class="d-flex align-items-left">
            <div class="user-chat-image d-flex flex-column align-items-center">
                <img class="u-image-con" src="/Images/user-icon.png" alt="user">
                <p id="chat-user" class="ms-1">${name}</p>
            </div>
            <span class="ms-2 p-1 left-text">${message}</span>
        </div>
    `;

    // Додавання нового повідомлення в кінець блока
    messageField.innerHTML += newMessage;
}

function addMessageToChatMy(data) {
    const messageField = document.getElementById('message-field');

    // HTML-код для додавання
    const newMessage = `  <div class="d-flex justify-content-end">
    <span class="ms-2 p-1 left-text" style="background: #9dafe0;"">${data}</span>
    <div class=" user-chat-image d-flex flex-column align-items-center">
      <img class="u-image-con" src="/Images/user-icon.png" alt="user">
      <p id="chat-user" class="ms-1">Me</p>
    </div>
  </div>`;

    // Додавання нового повідомлення в кінець блока
    messageField.innerHTML += newMessage;
}

let closeBut = $("#closeRoom");
let RoomForm = $("#new");

let NewRoomBut = $('#NewRoom')


NewRoomBut.on('click', function() {
    RoomForm.addClass("visible").removeClass("unvisible");
     $('#roomName').val('');
});


closeBut.on('click', function() {
    RoomForm.addClass("unvisible").removeClass("visible");
});


let CreateRoomBut = $('#CreateRoom')

CreateRoomBut.on('click', function(event) {
   
     event.preventDefault(); // Зупинка стандартної дії кнопки

     const rooms = $('#roomBlock');
    let roomName = $('#roomName').val();
    
     rooms.append(`<div class="some-room mt-2 ">
                      <img id="u-image" class="user-img" src="/Images/user-icon.png" alt="user">
                     <p class="u-name">${roomName}</p>
                  </div>`);


    RoomForm.addClass("unvisible").removeClass("visible");

    socket.emit('new-room', {roomName: roomName})
});


socket.on('add-new-room', room =>{
    const rooms = $('#roomBlock');
    rooms.append(`<div class="some-room mt-2 ">
    <img id="u-image" class="user-img" src="/Images/user-icon.png" alt="user">
   <p class="u-name">${room.roomName}</p>
</div>`);

})

let RoomName;
let userName = $('#nameUp').text();
// Встановлюємо обробник подій на батьківський елемент, який існує в DOM
$('#roomBlock').on('click', '.some-room', function() {
    console.log('натиснули');

    let block = $(this);
    RoomName = block.find('.u-name').text();
    console.log(RoomName);
    UpdateIcons(RoomName);

    const MessageArea = $('#message-field');
    MessageArea.empty();
    fetch('/allRooms')
    .then(response => response.json())
    .then(data => {
       
        data.forEach(room=> {
            if(room.roomName === RoomName) {
                room.messages.forEach(message=> {
                    if(message.senderName === username) {
                        addMessageToChatMy(message.text)
                    }
                    else {
                        addMessageToChatSomeoneElse(message.senderName, message.text)
                    }
                })
            }
        })
    })
    .catch(error => console.error('Error fetching data:', error));
    
     $('#chatName').text(`${RoomName}`)
    socket.emit('join-to-room', {roomName: RoomName, userName: username});           
});

function UpdateIcons(roomName) {
    let conteiner = $('#user-icon-conteiner');
    conteiner.empty();
    AllBdRooms.forEach(room=> {
        if(room.roomName === roomName) {
          room.participants.forEach(participant =>{
            console.log(participant)
            conteiner.append(` <div class="ms-2 user-chat-image d-flex flex-column align-items-center">
            <img class="u-image-con" src="/Images/user-icon.png" alt="user">
            <p id="chat-user" class="ms-1 name-in-chat">${participant.userName}</p>
         </div>`) 
          })
        }
    })

}


send_btn.onclick = function() {
    let mess = message.value;
    socket.emit('send-chat-message', {mess: mess, name:username, roomName: RoomName})
    message.value = '';
    addMessageToChatMy(mess); 
}

//
let AllBdRooms;
//

fetch('/allRooms')
    .then(response => response.json())
    .then(data => {
        AllBdRooms = data;
        console.log('Data received from server:', data);
        const roomsFromDB = $('#roomBlock');
        data.forEach(room => {
            roomsFromDB.append(`<div class="some-room mt-2 ">
                      <img id="u-image" class="user-img" src="/Images/user-icon.png" alt="user">
                     <p class="u-name">${room.roomName}</p>
                  </div>`);

        });
        // Тут ви можете виконати будь-які дії з отриманими даними
    })
    .catch(error => console.error('Error fetching data:', error));

