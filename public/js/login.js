document.addEventListener("DOMContentLoaded", function() {
    // Тут викликаємо функцію CheckUser()
document.getElementById("submitBtn").addEventListener("click", function(event) {
    event.preventDefault(); // Ця стрічка перешкодить відправці форми, доки не виконаються умови

    let password = document.getElementById("pwd").value;
    let Login = document.getElementById('uname').value;
    if (password.length == 0) {
        document.getElementById("val-div").textContent = "Please fill out this field.";
    }
    if(Login == 0) {
        document.getElementById("val-div2").textContent = "Please fill out this field.";
    }
    else if (password.length < 4) {
        document.getElementById("val-div").textContent = "Password must contain at least 4 characters";
    } else {
        CheckUser(); // Виклик функції для перевірки користувача
    }
});

let username;
function CheckUser() {
    let Login = document.getElementById('uname').value;
    let password = document.getElementById('pwd').value;
    let data = {
        login: Login,
        password: password
    }

    $.ajax({
        url: 'http://localhost:3000/Login.php',
        type: 'POST',
        data: data,
        success: function(response) {
            try {
                var parsedResponse = JSON.parse(response); // Розпарсити JSON відповідь
                if (parsedResponse.success) {
                    user = parsedResponse.user;
                    username = user.surname;
                    sessionStorage.setItem('username', username); // Зберегти логін користувача

                    console.log(user);                    
                    document.getElementById('myForm').submit();
                } else {
                    console.error("Error logging in: " + parsedResponse.message);
                    document.getElementById("val-div").textContent = "Wrong login or password";
                    document.getElementById("val-div2").textContent = "Wrong login or password";
                }
            } catch(error) {
                console.error('Error parsing JSON:', error);
                console.log('Response:', response);
            }
        },
        error: function(xhr, status, error) {
            console.error('Error sending request:', error);
        }
    });
}

});