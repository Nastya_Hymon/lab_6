const mongoose = require('mongoose');

const RoomMessageSchema = new mongoose.Schema({
   
    roomName:{
        type: String,
        unique: true
        //required: true
    },
    participants: [{
        userName: {
            type: String,
            unique: true
            //required: true
        }
    }],
    messages: [{
        senderName: {
            type: String,
           // required: true
        },
        text: {
            type: String,
           // required: true
        }
    }]
})
const RoomMessage = mongoose.model('RoomMessage', RoomMessageSchema)
module.exports = RoomMessage;