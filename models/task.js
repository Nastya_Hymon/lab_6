const mongoose = require('mongoose');

const TaskSchema = new mongoose.Schema({
   
    Board:{
        type: String,
        //required: true
    },
    Name:{
        type: String,
        //required: true
    },
    Date:{
        type: String,
        //required: true
    },
    Description:{
        type: String,
        //required: true
    },
})
const Task = mongoose.model('Task', TaskSchema)
module.exports = Task;