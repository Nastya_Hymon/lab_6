// self.addEventListener('install', function(event) {
//     console.log('[SW: install]');
// });


// self.addEventListener('activate', function(event) {
//     console.log('[SW: activate]');
// });

const CACHE_NAME = 'my-pwa-cache';
const urlsToCache = [
    'index.html',
    'header.css',
    'main.css',
    'modalWindows.css',
    'script.js',
    'server.php'
];

self.addEventListener('install', function(event) {
    event.waitUntil(
        caches.open(CACHE_NAME)
            .then(function(cache) {
                console.log('Opened cache');
                return cache.addAll(urlsToCache);
            })
    );
});

self.addEventListener('fetch', function(event) {
    event.respondWith(
        caches.match(event.request)
            .then(function(response) {
                if (response) {
                    return response;
                }

                const fetchRequest = event.request.clone();

                return fetch(fetchRequest).then(
                    function(response) {
                        if (!response || response.status !== 200 || response.type !== 'basic') {
                            return response;
                        }

                        const responseToCache = response.clone();

                        caches.open(CACHE_NAME)
                            .then(function(cache) {
                                cache.put(event.request, responseToCache);
                            });

                        return response;
                    }
                );
            })
    );
});

// Update cache when new service worker is activated
self.addEventListener('activate', function(event) {
    event.waitUntil(
        caches.keys().then(function(cacheNames) {
            return Promise.all(
                cacheNames.filter(function(cacheName) {
                    return cacheName.startsWith('my-pwa-cache-') && cacheName !== CACHE_NAME;
                }).map(function(cacheName) {
                    return caches.delete(cacheName);
                })
            );
        })
    );
});





// self.addEventListener('activate', (event) => {
//     event.waitUntil(
//     caches.keys().then((cacheNames) => {
//     return Promise.all(
//     cacheNames.filter((cacheName) => {
//     return cacheName.startsWith('my-pwa-cache-') &&
//     cacheName !== CACHE_NAME;
//     }).map((cacheName) => {
//     return caches.delete(cacheName);
//     })
//     );
//     })
//     );
//     });
    
